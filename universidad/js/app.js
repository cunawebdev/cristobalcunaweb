/*
--------CLASE 1 ------------



let elemento;
elemento    =  document;
elemento    =  document.all;
elemento    =  document.all[0];
elemento    =  document.head;
elemento    =  document.body;
elemento    =  document.domain;//regresa la url
elemento    =  document.URL;// regresa el protocolo del puerto 
elemento    =   document.characterSet;
elemento    =   document.forms;
elemento    =   document.forms[0];
elemento    =   document.forms[0].id;
elemento    =   document.forms[0].claseName;
elemento    =   document.forms[0].classList;
elemento    =   document.forms[0].classList[0];

elemento    =   document.images;
elemento    =   document.images[2];
elemento    =   document.images[2].getAttribute('src');

//scrips

elemento    =   document.scripts;
elemento    =   document.images;
let imagenes    =   document.images;
let imagenesArr =   Array.from(imagenes);//con este array from  puedes convertir un html collection en un arreglo.
imagenesArr.forEach(function(imagen){//esto sirve para que las mande por separado , la variable no influye
    console.log(imagen);
});
console.log(elemento);
 



// Eliminar de Local Storage
//localStorage.clear();
*/


/*------ CLASE 2--------
 let encabezado;
 encabezado= document.getElementById('encabezado');
 encabezado.style.background    =  '#333';//cuando se le pone style es para modificar el css de la pagina 
 encabezado.style.color =    '#fff';
 encabezado.style.padding   =   '20px';


 encabezado.innerText='Los mejores cursos ';// este sirve para sustituir un texto por otro (anteriormente Cursos en Linea)

 console.log(encabezado);
*/



/*                  ----- CLASE 3 ----- 

const encabezado = document.querySelector('#encabezado');// fuera una clase usas un ( . ) en vez de # ---- query selector tambien puede  seleccionar por etiquetas

//aplicacion de css

encabezado.style.background = '#333';
encabezado.style.color= '#fff';
encabezado.style.padding='20px';
encabezado.textContent='los mejores cursos';
console.log(encabezado);

 let enlace;
enlace= document.querySelector('#principal a:first-child'); //seleccionar elemento padre y un hijo si  quieres  unos de los que estan enmedio solo metes en el paretesis la posicion
enlace= document.querySelector('#principal a:nth-child(3)');
//enlace= document.querySelector('#principal a:last-child');

 console.log(enlace);
*/

/*                   ------ CLASE 4 ------

let enlaces = document.getElementsByClassName('enlace');//tambien puede ser ('enlace[n posicion])

enlaces = enlaces[3];

enlaces.style.background = '#333';
enlaces.textContent = 'NUEVO ENLACE';
console.log(enlaces);


MESCLAR QUERY SELECTOR  CON GET ELEMENT BY 


const listaEnlaces= document.querySelector('#principal').getElementsByClassName('enlace');

console.log(listaEnlaces);



const links = document.getElementsByTagName('a');
links[17].style.color='red';
links[17].textContent='Nuevo Enlace';

console.log(links);



const links = document.getElementsByTagName('a');

let enlaces = Array.from(links);
enlaces.forEach(function(enlace){
    console.log(enlace.textContent);
});
console.log(links);

*/


/* ----------------------CLASE 5 ------------------

const enlaces = document.querySelectorAll('#principal a:nth-child(odd)');// el (odd) es para que sea impar

enlaces.forEach(function(impares){
    impares.style.backgroundColor= 'red';
    impares.style.color='white';
   //impares.textContent='aaaaaaaaa';
});
console.log(enlaces);

*/

/*  ----------------CLASE 6  ------------------*/

/*
const navegacion = document.querySelector('#principal');

console.log(navegacion.children[0].textContent='nuevo enlace');


 1= Elementos
    2= Atributos
    3=Text Node
    8=comentario
    9 = documentos
    10 = doctype
*/


/*  AGREGANDO LEYENDO BORRANDO Y LIMPIANDO LOCAL STORE ------------------------------*/
// localStorage.setItem('nombre,juan');

//eliminar

//localStorage.removeItem('nombre');
/*
localStorage.setItem('nombre,juan');

const nombre = localStorage.getItem('nombre');

console.log(nombre);


localStorage.clear();

*/
